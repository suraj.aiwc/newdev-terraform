variable "sample" {
    default = "Hello world program has been changed"

}

output"sample" {
    value = var.sample
}


# in terraform single cotes are not allowed.


output "sample1" {
    value = "value is ${var.sample}"
}

variable "number" {
   default = 100
}

output number{
    value = var.number
}

variable "ex-list" {
    default = [
        "Devops" ,
        100 ,
        false ,
        "verma"
    ]
}

output "ex-list" {
    value = "welcome to ${var.ex-list[0]} Training , Trainer is ${var.ex-list[3]} , Training duration is ${var.ex-list[1]} hours"
    }
    
/*variable "CITY" {
  
      output CITY {
      value = "City is $(var.CITY)"
       }
  } */
     

   ## when you accessing a variable from a file make sure you need to 
   # declare that empty variable locally and then access.


     variable "CITY" {}  # this is empty variable.
output "CITY" {
    value = "City is ${var.CITY}"
}



    variable "STATE" {}  # this is empty variable.
output "STATE" {
    value = "Bangalore city is in ${var.STATE}"
}



