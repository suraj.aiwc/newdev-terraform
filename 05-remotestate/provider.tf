provider "aws" {
  region     = "us-east-2"
}



terraform {
  backend "s3" {
    bucket = "mynewdev"
    key    = "newdev/learning/terraform.tfstate"
    region = "us-east-2"
    dynamodb_table  = "terraform-locking"
  }
}



resource "aws_instance" "web" {
  ami                       = "ami-0a5588cee1fe39fff"
  instance_type             = "t2.micro"
  vpc_security_group_ids    = ["sg-0f24e05b4a427c1c6"]
  key_name                  = "new Dev"

  tags = {
    Name    = "EC2-From-Terraform"
    Env     = "Dev"
    Batch   = "42"
    Trainer = "suraj k"
  }
}